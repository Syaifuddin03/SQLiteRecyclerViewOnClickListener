package com.example.sqliterecyclerviewonclicklistener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {


    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<Model> item;

    public MyAdapter(Activity activity, ArrayList<Model> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Model getItem(int position) {
        return item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_gambar, null);

            Model model = item.get(position);

            TextView mPosisi = convertView.findViewById(R.id.rvPosisi);
            ImageView mImage = convertView.findViewById(R.id.rvImage);

            mPosisi.setText(model.getPosisi());
            mImage.setImageBitmap(model.getPhoto());

        }
        return convertView;
    }
}
