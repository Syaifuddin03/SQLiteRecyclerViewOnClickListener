package com.example.sqliterecyclerviewonclicklistener;

import android.view.View;

interface ItemClickListener {
    void onItemClickListener(View v, int position);
}
