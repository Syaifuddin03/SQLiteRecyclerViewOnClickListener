package com.example.sqliterecyclerviewonclicklistener;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class DatabaseClass extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "banjariapp.db";
    private static final int DATABASE_VERSION = 1;
    private final Context context;
    private Bitmap bitmap;


    public DatabaseClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    // Get Data From Database
    public ArrayList<Model> getAllData()
    {
        try
        {
            ArrayList<Model> objModelArrayList =new ArrayList<>();
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            if (sqLiteDatabase != null)
            {
                Cursor mCursor =sqLiteDatabase.rawQuery("select * from banjaridata", null);
                if (mCursor.getCount() != 0)
                {
                    while (mCursor.moveToNext())
                    {
                        String pPosisi = mCursor.getString(0);
                        byte[] iImage = mCursor.getBlob(1);

                        Bitmap bImage = BitmapFactory.decodeByteArray(iImage, 0, iImage.length);
                        objModelArrayList.add(new Model(pPosisi, bImage));
                    }
                    return objModelArrayList;
                }
                else
                {
                    Toast.makeText(context, "No data is Retrevied...", Toast.LENGTH_SHORT).show();
                    return null;
                }
            }
            else
            {
                Toast.makeText(context, "Data Is Null", Toast.LENGTH_SHORT).show();
                return null;
            }
        }
        catch (Exception e)
        {
            Toast.makeText(context, "getAllData "+ e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }
}
