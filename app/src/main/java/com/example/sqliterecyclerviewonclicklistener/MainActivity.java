package com.example.sqliterecyclerviewonclicklistener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    DatabaseClass databaseClass;
    ArrayList<Model> objModelsArrayList = new ArrayList<>();
    ListView listView;
    Model model;
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.recycler_view);

        databaseClass = new DatabaseClass(this);
        objModelsArrayList = databaseClass.getAllData();
        myAdapter = new MyAdapter(this, objModelsArrayList);
        listView.setAdapter(myAdapter);
        listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{

                    // Intent Put Extra

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Intent"+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}
